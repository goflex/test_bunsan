package main

import (
	"bufio"
	"fmt"
	"os"
)

// check Verifica si el objeto de error recibido es diferente de vacío, si es positivo manda un panic
func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {

	// Open the file layout with the entries for numbers to read
	file, err := os.Open("layout.txt")
	check(err)

	defer file.Close()

	// We scann the file
	scanner := bufio.NewScanner(file)

	// Get line by line of file
	scanner.Split(bufio.ScanLines)
	// Each position is a line of file
	var text []string

	for scanner.Scan() {
		text = append(text, scanner.Text())
	}

	// Clean text of line breaks, or lines with length > 27 || < 27
	textFile := []string{}
	for _, t := range text {
		if len(t) == 27 {
			// Insert in new clean slice
			textFile = append(textFile, t)
		}
	}

	/* Platicábamos sobre los patrones de diseño, aquí usamos de forma básica el patrón de facade,
	el cual nos permite concentrar la lógica dividida en diferentes funciones, para que el código
	pueda ser reutilizable y fácil de mantener */
	resNumbers := readNumbers(textFile)
	r := checkSum(resNumbers)

	fmt.Println("Muchas gracias por correr este programa!")
	fmt.Println("ATENTAMENTE: J. Damián Jiménez Navarro")

	fmt.Println(`NOTA: Los resultados son impresos por pantalla, 
	pero también guardados en un archivo txt llamado results.txt.
	RESULTADO:`)

	// Creating the file to write
	f, err := os.Create("results.txt")
	check(err)
	defer f.Close()

	// Printing for screen and writting in the file
	for i := 1; i <= len(r); i++ {
		fmt.Printf("%v \t %v\n", r[i][0], r[i][1])

		// Writting in the file results
		_, err := f.WriteString(fmt.Sprintf("%v \t %v\n", r[i][0], r[i][1]))
		check(err)
	}
}
