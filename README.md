# test_bunsan

A test for technical skills, A development to bunsan

## Instrucciones para ejecutar el programa

- Tener instalado Go 1.17 o superior
- Clonar este repositorio en carpeta local
- Ingresar a la carpeta donde fue clonado el código
- Ejecutar el siguiente comando : _**go run .**_
- Listo

## Instrucciones para generar un ejecutable del programa

Si se desea generar un ejecutable (Portable) del programa realizar lo siguiente:
- Tener instalado Go 1.17 o superior
- Clonar este repositorio en carpeta local
- Ingresar a la carpeta donde fue clonado el código
- Ejecutar el siguiente comando para generar el ejecutable: _**go build .**_
- Correr el ejecutable con: _**./test_bunsan**_
- Listo

## Estructura del sistema
- **main.go** Es el archivo principal que desencadena la ejecución del sistema
- **utilities.go** Aquí se almacenan las funciones principales para convertir el texto del archivo a cantidad así como también la validación de checksum
- **anotaciones_proceso.txt** Algunas anotaciones que hice para entender y darle solución al problema
- **go.mod** Archivo generado por Go de forma automática al inicializar el módulo (go init mod)
- **layout.txt** Archivo que contiene las entradas a leer por el sistema para que las convierta en cantidades y luego valide
- **README.md** Contiene información e instrucciones del sistema

> Muchas gracias por el tiempo dado a la revisión de esta prueba técnica!

