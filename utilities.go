package main

import (
	"math"
	"sort"
	"strconv"
)

// readNumbers
// Obtiene el archivo leido en formato string y lo procesa, retornando los posibles valores númericos
// hayados dentro del archivo
func readNumbers(textFile []string) map[int][]string {
	/*   "123"   "12"   "123"   "12"   "123"   "123"   "123"   "23"   "123"   "123"
	a := " _ " + "  " + " _ " + "_ " + "   " + " _ " + " _ " + "_ " + " _ " + " _ "
	b := "| |" + " |" + " _|" + "_|" + "|_|" + "|_ " + "|_ " + " |" + "|_|" + "|_|"
	c := "|_|" + " |" + "|_ " + "_|" + "  |" + " _|" + "|_|" + " |" + "|_|" + " _|"
	 _    _ _     _  _ _  _  _
	| | | _|_||_||_ |_  ||_||_|
	|_| ||_ _|  | _||_| ||_| _|
	*/

	// Here we save the structure of each number to process it
	numberInMap := map[int][]string{
		0: {" _ ", "| |", "|_|", "0"}, // 0
		1: {"  ", " |", " |", "1"},    // 1
		2: {" _ ", " _|", "|_ ", "2"}, // 2
		3: {"_ ", "_|", "_|", "3"},    // 3
		4: {"   ", "|_|", "  |", "4"}, // 4
		5: {" _ ", "|_ ", " _|", "5"}, // 5
		6: {" _ ", "|_ ", "|_|", "6"}, // 6
		7: {"_ ", " |", " |", "7"},    // 7
		8: {" _ ", "|_|", "|_|", "8"}, // 8
		9: {" _ ", "|_|", " _|", "9"}, // 9
	}

	// Aquí iremos guardando los números resultantes de la lectura de cada entrada dentro del archivo!
	resNumbers := map[int][]string{}

	iNumbers := 1
	// Iteramos sobre los "grupos de caracteres" que forman posibles cantidades numéricas
	for {
		if len(textFile) < 3 {
			// Esto significa el final del proceso, poruqe ya no hay líneas de dígitos que leer
			break
		}

		// Iteramos sobre la línea horizontal en búsqueda de los números (deben ser 27 caracteres)
		for {
			if len(textFile[0]) <= 0 || len(resNumbers[iNumbers]) == 9 {
				// Termina y buscamos una nueva triada de líneas a leer
				// Eliminación de las 3 líneas que forman el "grupo de caracteres"
				textFile = textFile[3:]
				break
			}

			var sizeChars int
			isMatching := []bool{false, false, false}

			// FOR Iteramos sobre cada uno de los 9 números para encontrar coincidencias
			for j := 0; j < 10; j++ { // Aquí estamos recorriendo los números del 0 al 9
				// Obtenemos el len de la primera posición de los caracteres que lo definen
				sizeChars = len(numberInMap[j][0])
				// Get the len of number in the map y con ello tomamos las posiciones en el slice de bytes (Horizontal)

				isMatching = []bool{false, false, false}
				for i := 0; i < 3; i++ { // recorriendo líneas verticales del textFile (Siempre son 3, pues es el número de líneas que constituye a un número)
					if textFile[i][:sizeChars] == numberInMap[j][i] { // Preguntamos si los caracteres del número coincide con el leído desde el texto
						isMatching[i] = true
					} else {
						isMatching[i] = false
					}
				}

				if isMatching[0] && isMatching[1] && isMatching[2] { // Si coincidió el número lo guardamos en el mapa de resNumbers
					if _, ok := resNumbers[iNumbers]; ok {
						resNumbers[iNumbers] = append(resNumbers[iNumbers], numberInMap[j][3])
					} else {
						resNumbers[iNumbers] = append([]string{}, numberInMap[j][3])
					}

					// Eliminamos las posiciones del slice de bytes (horizontales) por cada línea (vertical) pues ya fueron leídas y procesadas, y coincidió, por eso ya no seguimos
					for i := 0; i < 3; i++ { // recorriendo líneas verticales del textFile (Siempre son 3, pues es el número de líneas que constituye a un número)
						textFile[i] = textFile[i][sizeChars:]
					}

					// Salimos del recorrido de números, pues ya no es necesario porque hubo coincidencia!
					break
				}
			}

			if !isMatching[0] || !isMatching[1] || !isMatching[2] {
				// Llega aquí porque no hubo coincidencia con ninguno de los números del 0 - 9
				if _, ok := resNumbers[iNumbers]; ok {
					resNumbers[iNumbers] = append(resNumbers[iNumbers], "?")
				} else {
					resNumbers[iNumbers] = append([]string{}, "?")
				}

				// Eliminamos las posiciones del slice de bytes (horizontales) por cada línea (vertical) pues ya fueron leídas y procesadas, aunque no coincidió :(
				for i := 0; i < 3; i++ { // recorriendo líneas verticales del textFile (Siempre son 3, pues es el número de líneas que constituye a un número)
					if len(textFile[i]) >= 3 {
						textFile[i] = textFile[i][3:]
					} else {
						// Colocamos cadena vacía para que resulte en len vacío y ya no vuelva a entrar en el for
						textFile[i] = ""
					}
				}
			}
		}

		iNumbers++ // Representa el número de entradas
	}

	return resNumbers
}

// checkSum
// Calcula la suma de verificación para un número de cuenta determinado e
// identifiqua si se trata de un número de cuenta válido.
// 	Cada número lo retorna dentro de una posición en un tipo de dato map[int][]interface{}
func checkSum(quantities map[int][]string) map[int][]interface{} {
	res := map[int][]interface{}{}
	posCheckSum := []int{9, 8, 7, 6, 5, 4, 3, 2, 1}

	for j, q := range quantities {
		/* Buscamos un question mark en el slice, si aparecer marcamos la cantidad con el identificador
		ILL y saltamos a la siguiente posición en busca de una nueva cantidad a validar */
		qm := sort.SearchStrings(q, "?")
		if qm < len(q) {
			res[j] = append([]interface{}{}, q)
			res[j] = append(res[j], "ILL")
			continue
		}

		// No encontramos "?" continuamos con la validación
		var sum int
		// Hacemos el recorrido de las posiciones para cumplir con la fórmula de checksum
		// (1*d1 + 2*d2 + 3*d3 + ... + 9*d9)
		for i, ch := range posCheckSum {
			num, _ := strconv.Atoi(q[i])
			sum += ch * num
		}

		// Mod shoud to be 0 for a valid quantity
		if math.Mod(float64(sum), 11) == 0 {
			res[j] = append([]interface{}{}, q)
			res[j] = append(res[j], "OK")
		} else { // The mod was different to 0
			res[j] = append([]interface{}{}, q)
			res[j] = append(res[j], "ERR")
		}
	}

	return res
}
